Ipktgen is a realistic packet generator. It uses ML algorithms and
statistical distributions to create a model for network traffic seen in an
input pcap file. The model generated, can then taken to a different network and
be used to generate traffic that is very similar to what was seen in the
original capture file.

Full documentation can be found at [http://docs.uh-netlab.org/ipktgen/index.html](http://docs.uh-netlab.org/ipktgen/index.html).