================================
USAGE
================================
TO EXTRACT TRAFFIC MODEL FROM PCAP
    python genmodel.py -f pcapfile_path --o output_dir_for_model -n netdata_json_file_path -v

TO GENERATE PACKETS FROM JSON FOR SPECIFIC APP AS CLIENT 
    python genpackets.py -v -t clients -a  app_model.json -n net_model.json -i local_ip_address

TO GENERATE PACKETS FROM JSON FOR SPECIFIC APP AS SERVER 
    python genpackets.py -v -t clients -a  app_model.json -n net_model.json -i local_ip_address

==================================
CHANGES IN THIS VERSION
==================================
Version v0006
  creating the model from datasets files generated .: seprating gendataset.py
  from genmodel.py. .: gives more flexibility, now we can generate dataset just
  once, and reuse to create models each net time

Version v0004
  generating datasets first and saving them to file before creating model.

Version v0003
...

Version v0002
...

Version v000


from selenium import webdriver
import os 
import time
import re
import shutil


CHROMEDRIVER_PATH = '/usr/local/bin/chromedriver'
GOOGLE_CHROME_SHIM = os.getenv('GOOGLE_CHROME_SHIM',"chromedriver")
chrome_options = webdriver.ChromeOptions()

chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')
chrome_options.add_argument("--privileged")
chrome_options.add_argument("--disable-gpu")
chrome_options.add_argument("--no-default-browser-check")
chrome_options.add_argument("'--no-first-run'")
chrome_options.binary_location = '/usr/bin/chromium-browser'
chrome_options.add_argument("--start-maximized")
chrome_options.add_argument("--disable-infobars")
chrome_options.add_argument("--disable-extensions")
options.add_argument("download.default_directory=/Users/oadeleke/all_papers")

#driver = webdriver.Chrome('/usr/bin/chromedriver', chrome_options=chrome_options)
driver = webdriver.Chrome('/usr/local/bin/chromedriver', chrome_options=chrome_options)


driver.get('https://dl-acm-org.ezproxy.lib.uh.edu/proceedings.cfm')
time.sleep(5)
driver.find_element_by_link_text("ACM SIGCOMM").click()
#driver.get("https://dl.acm.org/event.cfm?id=RE258")
time.sleep(2)
driver.find_element_by_id('tab-1019').click()
time.sleep(2)
all_procs = driver.find_elements_by_partial_link_text("Proceedings")
all_proci = [(purl.text, purl.get_attribute("href")) for purl in all_procs]
i = 0

if not os.path.exists('all_papers'):
    os.makedirs('all_papers')

for purl in all_proci:
  print "working on - ", purl[0]
  if '-X' in purl[0] or '-IX' in purl[0]:
    xx = re.split("-|:",purl[0])
  else:
    xx = re.split("\'|:",purl[0])
  
  short_cname, yr, long_cname = xx[0], xx[1], xx[2]
  
  short_cname = short_cname.strip()
  if not os.path.exists('all_papers/' + short_cname):
    os.makedirs('all_papers/' + short_cname)  
  if not os.path.exists('all_papers/' + short_cname + '/' + yr):
    os.makedirs('all_papers/' + short_cname + '/' + yr)
  
  driver.get(purl[1])
  time.sleep(5)
  
  try:
    driver.find_element_by_link_text("single page view").click()
  except:
    print 'cant find single page view'
  time.sleep(3)

  all_paps = driver.find_elements_by_link_text("PDF")
  all_paps[5].click()

  time.sleep
  dirpath = 'all_papers/'
  dstpath = 'all_papers/' + short_cname + '/' + yr
  filename = max([f for f in os.listdir('all_papers')], key=os.path.getctime)
  shutil.move(os.path.join(dirpath,filename),os.path.join(dstpath, filename))
  
  i += 1
  if i == 20:
    break


