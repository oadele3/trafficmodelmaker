#! /usr/bin/env python
"""genmodel.py creates a model to decribe all apps seen in an input trace file.

It uses ML and statistical distributions to create a model for network traffic
seen in aninput pcap file. The model generated, can then taken to a different
network andbe used to generate traffic that is very similar to what was seen in
the original capture file.

  Typical usage example:

  genmodel.py  -v -p pcapfile -o output-directory  \
  -m model-template

"""
import os
import logging
import time
import json
import numpy as np
import pandas as pd
from trafficdatasetmaker.trafficdatasetmaker import TrafficDatasetMaker
from appmodelmaker_generic import AppModelMakerGeneric

logger = logging.getLogger(__name__)

class Trafficmodelmaker():
    '''class to make traffic model from capture file'''
    def __init__(self, inputfile, outdir, inputfiletype='packets-csv',
                 metadatafile=None, modeltype='Generic',):
        #print (logger, logger.handlers)
        allowed_inputfiletypes = ['packets-csv', 'pcap']
        assert (inputfiletype in allowed_inputfiletypes), \
            'value of inputfiletype must be in ' + str(allowed_inputfiletypes)
        self.inputfile = inputfile
        self.outdir = outdir
        self.inputfiletype = inputfiletype
        self.metadatafile = metadatafile
        self.modeltype = modeltype.capitalize()
        if not os.path.exists(outdir):
            os.makedirs(outdir)

    def make_models(self):
        #from pcap file, make csv, get models for each app, & a net model
        logger.debug("started making model, may take some minutes")
        start = time.time()
        #has to be done before any step
        self.make_metadata_if_none()
        
        orig_meta_data = self.get_metadata()
        if self.inputfiletype == 'pcap':
            TrafficDatasetMaker(self.inputfile, self.outdir, self.inputfiletype,
                    self.metadatafile, ['all']).makecsvs()
            pkts = pd.read_csv(self.outdir + '/all_pkts.csv')
        else: #if inputfiletype == 'packets-csv'
            pkts = pd.read_csv(self.inputfile)
        metadat = self.get_metadata()

        if 'applications' in orig_meta_data:
            pkts = pkts[pkts['app_idx'].isin(orig_meta_data['applications'])]
        if 'l4_conversation_classes' in orig_meta_data:
            pkts = pkts[pkts['conn_class_idx'].isin(orig_meta_data['l4_conversation_classes'])]
        if 'l4_conversations' in orig_meta_data:
            pkts = pkts[pkts['l4_sess_idx'].isin(orig_meta_data['l4_conversations'])]

        apps = pkts.app_idx.dropna().unique()
        logger.debug("%d applications found, making models", len(apps))
        app_models = {}
        net_xtics = []
        for app_idx in apps:
            #if app_idx not in metadata['applications']:
            #    continue
            logger.debug("making model for app %s", app_idx)
            app_pkts = pkts.loc[pkts['app_idx'] == app_idx]
            app_pkts = app_pkts.loc[(pkts.l4_is_retransmission!=1)]
            if self.modeltype == 'Generic':
                appmodeler = AppModelMakerGeneric(app_idx, app_pkts)
            else:
                raise ValueError('invalid model type selected')
            app_model, net_x = appmodeler.build_model()
            app_models[app_idx] = app_model
            net_xtics.extend(net_x) 

        with open(self.outdir + '/app_model.json', 'w') as fp:
            json.dump(app_models, fp, indent=2)

        net_xtics.sort(key=lambda x:x['start'])
        with open(self.outdir + '/net_xtic.json', 'w') as fp:
            json.dump(net_xtics, fp, indent=2)

        self.update_metadata_file(pkts, metadat)

        dur = time.time() - start
        logger.debug("finished making models in %d", dur)



    def make_metadata_if_none(self):
        if self.metadatafile:
            pass
        else:
            self.metadatafile = self.outdir + '/metadata.json'
            dat = {}
            json.dump(dat, open(self.metadatafile, 'w'))

    def get_metadata(self):
        with open(self.metadatafile, 'r') as met:
            dat = json.load(met)
        return dat

    def update_metadata_file(self, pkts_df, dat):
        #do updates
        json.dump(dat, open(self.metadatafile, 'w'))
