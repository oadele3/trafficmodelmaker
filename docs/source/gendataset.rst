.. Copyright (c) 2018  Oluwamayowa Adeleke
.. ipktgen documentation master file

Dataset Extraction From Tracefiles
==================================

Usage
-----
This tool processes packets in a pcap file, removes all Layer 3 and 4 protocol-
specific recovery actions, reducing the application traces to pure application
data exchanges between these end points, and determined the application layer
protocols for each session. The output is a dataset of 5 files with individual
records for each packet, application layer data unit (PDU) and each session
that is found in the capture. This dataset can be further used by our modelling
tool for traffic model creation :doc:`genmodel`. 

To generate a dataset from pcaps use the command from the ipktgen directory::

    python ./ipktgen/v0007/gendataset.py -v -f pcapfile -o ./outputdirectory

This command will create 4 files into './outputdirectory'. The detailled
description of the dataset created is given below.

Dataset Description
-------------------
Currently, the gendataset tool creates 5 files into the ./outputdirectory
specified in the command. The files are: 

* all_apps.csv, 
* all_sessions.csv,
* all_pdus.csv, 
* defrag_pkts.csv, 
* all_pkts.csv

details of the contents of each file are given below. A sample of a pcap and
its corresponding extracted dataset can also be found at >>>>>>> add link
<<<<<<<<


all_apps.csv
^^^^^^^^^^^^
This contains records for all appplications detected for all sessions. This
file has only 1 column.

::

    app_idx:  unique 'name' of each app cluster seen in the pcap 

all_pdus.csv
^^^^^^^^^^^^
records for all application level protocol data units (PDUs). The file has
columns.

::

    pdu_idx:            unique index of the pdu within its session 
    time:               time when the first packet of pdu was seen 
    l4_proto:           \'tcp' or /'udp' 
    app_idx:            unique 'name' for the pdu's detected app / cluster
    sess_idx:           unique index of the tcp or udp session pdu belongs to
    src_mac:            source mac of the first packet of pdu 
    dst_mac:            destination mac of the first packet of pdu
    src_ip:             source IP of the pdu 
    dst_ip:             destination IP of the pdu 
    src_port:           source port number of the pdu  
    dst_port:           source port number of the pdu  
    d_size:             size in bytes of the pdu i.e. layer 4 payload
    data:               contents of the pdu i.e. layer 4 payload, if available
    from_client:        'TRUE' if pdu is detected to come from TCP or UDP
                        client.
    ipt_sameside:       time interval between this pdu and the previous sent
                        from same endpoint of the session
    idx_sameside:       index of this pdu relative to other pdus sent from the
                        same endpoint of the session
    sess_time:          session start time for the session in which this pdu
                        was generated
    last_request_idx:   (for pdus detected to be from serverside) index of the
                        latest pdu from client side
    last_request_size:  (for pdus detected to be from serverside) data size of
                        the latest pdu from client side
    response_time:      (for pdus detected to be from serverside) time-interval
                        the last received pdu and this pdu

all_sessions.csv
^^^^^^^^^^^^^^^^
This file has records for all UDP and full TCP sessions in the pcapfile. it has
the colums below.

::

    sess_idx:               unique index of the tcp or udp session
    type:                   'tcp' or 'udp'
    ref_key:                string in format:
                            "('src_ip', 'src_port', 'dst_ip', 'dst_port')"
                            example: ('10.0.1.1', '33518', '10.0.1.101', '80')
    start_time              time when the first packet of session was seen 
    app_idx                 unique 'name' for the sessions's detected app/cluster
    server_ip               IP address of the side detected to be the 'server'
    server_port             port number of the side detected to be the 'server'
    client_ip               IP address of the side detected to be the 'client'
    client_port             port number of the side detected to be the 'client'
    cli_mac                 mac address of the side detected to be the 'client'
    srv_mac                 mac address of the side detected to be the 'server'
    duration                time between first and last packet of the session
    num_push                number of packets in session with psh flags
    num_ack                 number of packets in session with ack flags
    num_pdus                number of application level Protocol data Units
                            (pdu) in session
    num_cli_pdus            number of pdus originating from client in session
    num_srv_pdus            number of pdus originating from server in session
    total_bytes             total data in bytes exchanged in the session
    total_bytes_from_cli    data in bytes originating from 'client' in the session
    total_bytes_from_srv:   data in bytes originating from 'server' in the session

defrag_pkts.csv
^^^^^^^^^^^^^^^
records for all packets after ip-reassembly
This contains records for all complete packets in the pcapfile. the columns
include

::

WORK IN PROGRESS
'pkt_idx', 'defrag_pkt_idx', 'time',
'sess_idx', 'is_duplicate', 'sess_pdu_idx',
'app_idx', 'src_mac', 
'dst_mac', 'l2_d_size', 'l3_type', 
'src_ip', 'dst_ip', 'frag_idx',
'dont_frag','more_frags', 'frag_off',
'l3_d_size', 'l4_type', 'src_port',
'dst_port', 'seq_num', 'ack_num',
'urg', 'ack', 'psh',
'rst','syn', 'fin', 
'win_size','data_off', 'd_size',
'data'])

all_pkts.csv
^^^^^^^^^^^^
(WORK IN PROGRESS)


