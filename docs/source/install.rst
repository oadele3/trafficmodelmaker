.. Copyright (c) 2018  Oluwamayowa Adeleke
.. ipktgen documentation master file

Installing Ipktgen
==================

Requirements
------------
Before downloading and using ipktgen, ensure that you have the following
python libraries installed on the device on which you intend to run ipktgen:
dpkt, numpy, pandas, scipy, scikit-learn and matplotlib.

all these can be installed fom pip using the command::

    # pip install dpkt numpy pandas scipy scikit-learn matplotlib

Downloading Ipktgen
-------------------

Ipktgen sources can be downloaded from bitbucket using::

    # hg clone https://bitbucket.org/oadele3/ipktgen/

Once downloaded, Ipktgen can be used as demonstrated in :doc:`quickstart`
