import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(name='trafficmodelmaker',
      version='0.0.0.2',
      description='intelligent traffic model creation software',
      long_description=long_description,
      long_description_content_type="text/markdown",
      url='https://bitbucket.org/oadele3/trafficmodelmaker',
      author='UH-Netlab, Oluwamayowa Adeleke',
      author_email='oluwamayowa.adeleke@gmail.com',
      packages=setuptools.find_packages(),
      scripts=['bin/trafficmodelmaker'],
      include_package_data=True,
      zip_safe=False,
      install_requires=['dpkt', 'numpy', 'pandas', 'matplotlib', 'pcap2csv', 'jenkspy', 'sklearn', 'jenks_natural_breaks', 'kneed'],
      classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: Console",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
        ],
      python_requires='>=2.7',
      )