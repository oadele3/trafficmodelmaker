.. Copyright (c) 2018  Oluwamayowa Adeleke
.. ipktgen documentation master file

Welcome to the documentation for Ipktgen
========================================

Overview
--------
Ipktgen is a realistic packet generator. It uses ML algorithms and
statistical distributions to create a model for network traffic seen in an
input pcap file. The model generated, can then taken to a different network and
be used to generate traffic that is very similar to what was seen in the
original capture file.

Getting Started
---------------
.. toctree::
   :maxdepth: 1

   install
   quickstart

Further Usage and Implementation Details 
----------------------------------------
.. toctree::
   :maxdepth: 1

   gendataset
   genmodel
   genpkts
   gencompare

Sample Experiments
------------------
.. toctree::
   :maxdepth: 2

   experiments/index
