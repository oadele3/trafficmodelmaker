'''this module helps create generic app model from an input pcap file.
TO DO: add more detailed description'''

import logging
import pandas as pd
import numpy as np
from appmodelmaker_base import Bestmodel, Allmodel
from pcap2csv.pcap2csvutils import Pcap2csvutils

 #replace nunique_conn_pools with nunique_conns 
LOGGER = logging.getLogger(__name__)
USESS_DURATION_INFINITE_THRESHOLD = 10 #secs
CONN_POOL_INTERVAL_THRESHOLD = 0.5 #secs
#TODO: place consts in seprate .py file

class AppModelMakerGeneric():
    '''generic client server application model'''
    def __init__(self, app_name, pkts):
        
        self.app_pkts = pkts
        self.app_name = app_name
        self.app_pdus = Pcap2csvutils().pdus_from_pkts(self.app_pkts)

        self.app_model = {}
        self.net_xtic = []



    def build_model(self):
        '''calculate model params to meet generic client server app template'''
        LOGGER.debug( 'started building model for app %s',
                    str(self.app_name))
        self.app_model['app_name'] = self.app_name
        conn_classes = self.app_pkts.conn_class_idx.dropna().unique().tolist()
        conn_classes_models = {}
        for conn_class in conn_classes:
            conn_classes_models[conn_class] = self.model_conn(conn_class)
        self.app_model['conn_models'] = conn_classes_models
        self.getNetModelXtics()
        LOGGER.debug('model building complete for app %s ',
                    str(self.app_name))
        return self.app_model, self.net_xtic

    def model_conn(self, conn_class):
        LOGGER.debug( 'started building model for app %s, connection type %s',
                    str(self.app_name), conn_class)
        conn_class_pkts = self.app_pkts.loc[
                    self.app_pkts['conn_class_idx'] == conn_class]
        LOGGER.debug('connections in conn class %s are connections: %s', 
                     conn_class, str(conn_class_pkts.l4_sess_idx.unique().tolist()))
        conn_model = {}
        conn_model['port_number'] = self.get_app_port_number(conn_class_pkts)
        conn_model['l4_proto'] = self.get_l4_proto(conn_class_pkts)
        conn_model['duration'] = self.model_conn_duration(conn_class_pkts)
        conn_model['num_requests'] = self.model_conn_num_requests(conn_class_pkts)
        conn_model['request_sizes'] = self.model_conn_request_sizes(conn_class_pkts)
        conn_model['response_sizes'] = self.model_conn_response_sizes(conn_class_pkts)
        conn_model['num_response_per_req'] = self.model_num_responses_per_req(conn_class_pkts)
        conn_model['inter_request_times'] = \
            self.model_conn_inter_request_times(conn_class_pkts)
        conn_model['server_processing_times'] = \
            self.model_conn_server_processing_times(conn_class_pkts)
        conn_model['first_conn_start_time'] = \
            self.model_first_conn_class_starttime_in_usess(conn_class_pkts)
        conn_model['num_conn_pools'] = self.model_num_conn_pools(conn_class_pkts)
        conn_model['size_conn_pools'] = self.model_num_conn_in_pool(conn_class_pkts)
        conn_model['has_overlapping_conn_pools'] = \
                self.has_overlapping_conn_pools(conn_class_pkts)
        conn_model['start_start_intervals'] = \
                self.model_conn_start_starts_intervals(conn_class_pkts)
        conn_model['end_start_intervals'] = \
                self.model_conn_ends_starts_intervals(conn_class_pkts)
        conn_model['close_wait'] = \
                self.model_conn_close_wait(conn_class_pkts)
        LOGGER.debug('model for app %s, connection type %s completed',
                    str(self.app_name), conn_class)
        return conn_model
      
    def getNetModelXtics(self):
        #get app servers
        app_servers = self.app_pkts.loc[self.app_pkts['l4_from_client']==False].\
            l3_src_ip.unique().tolist()
        if 'ff.ff.ff.ff.ff.ff' in app_servers:
            app_servers.remove('ff.ff.ff.ff.ff.ff')
        if 'FF.FF.FF.FF.FF.FF' in app_servers:
            app_servers.remove('FF.FF.FF.FF.FF.FF')
        #get app client times
        for usess in self.app_pkts.user_sess_idx.dropna().unique().tolist():
            usess_pkts = self.app_pkts.loc[self.app_pkts['user_sess_idx']==usess]
            usess_pkts = usess_pkts.sort_values(by=['rel_time'])
            start = usess_pkts.iloc[0].rel_time
            end = usess_pkts.iloc[-1].rel_time
            r = usess_pkts.iloc[0]
            if r.l4_from_client == True:
                cli_ip, srv_ip = r.l3_src_ip, r.l3_dst_ip
                cli_mac, srv_mac = r.l2_src_mac, r.l2_dst_mac
            else:
                cli_ip, srv_ip = r.l3_dst_ip, r.l3_src_ip
                cli_mac, srv_mac= r.l2_dst_mac, r.l2_src_mac
            #print (cli_mac)
            #print(srv_mac)
            sess_desc = {'start':start,
                         'end':end,
                         'cli_ip':cli_ip,
                         'srv_ip':srv_ip,
                         'app':self.app_name}
            self.net_xtic.append(sess_desc)

    def get_app_port_number(self, conn_class_pkts):
        pnum = -1
        nconn_class_pkts = conn_class_pkts[conn_class_pkts.l4_sess_idx.notnull()]
        for index, row in nconn_class_pkts.iterrows():
            if row['l4_from_client']:
                pnum = row['l4_dst_port']
            else:
                pnum = row['l4_src_port']
            break
        return int(pnum)

    def get_l4_proto(self, conn_class_pkts):
        if len(conn_class_pkts) > 0:
            return self.app_pkts.iloc[0].l4_type
        return None

    def model_conn_duration(self, conn_class_pkts):
        '''get model for time duration of connections'''
        LOGGER.debug('modeling conn_duration')
        dset = []
        conn_pool_idxxs = conn_class_pkts.conn_pool_idx.dropna().unique().tolist()
        for cp in conn_pool_idxxs:
            cp_pkts = conn_class_pkts.loc[
                                conn_class_pkts['conn_pool_idx'] == cp]
            cp_pdus = Pcap2csvutils().pdus_from_pkts(cp_pkts)
            #TODO: use PDUs vs PKTs
            conn_duration = cp_pdus.rel_time.max() - cp_pdus.rel_time.min()
            dset.append(conn_duration)
        #return Bestmodel().getFit(dset)
        return Allmodel().getFit(dset)

    def model_conn_num_requests(self, conn_class_pkts):
        '''get model for number of requests per connection'''
        LOGGER.debug('modeling connction durations')
        dset = []
        l4_sess_idxs = conn_class_pkts.l4_sess_idx.dropna().unique().tolist()
        for conn in l4_sess_idxs:
            conn_pkts = self.app_pkts.loc[
                                self.app_pkts['l4_sess_idx'] == conn]
            conn_pdus = Pcap2csvutils().pdus_from_pkts(conn_pkts)
            num_conn_reqs = len(conn_pdus.loc[conn_pdus['l4_from_client'] == True])
            LOGGER.debug('conn %d has %d requests', conn, num_conn_reqs)
            dset.append(num_conn_reqs)
            #TODO: Ask use 1 conn per pool
        #return Bestmodel().getFit(dset)
        return Allmodel().getFit(dset)

    def model_conn_request_sizes(self, conn_class_pkts):
        LOGGER.debug('modeling connection request sizes')
        dset = []
        l4_sess_idxs = conn_class_pkts.l4_sess_idx.dropna().unique().tolist()
        for conn in l4_sess_idxs:
            conn_pkts = self.app_pkts.loc[
                                self.app_pkts['l4_sess_idx'] == conn]
            conn_pdus = Pcap2csvutils().pdus_from_pkts(conn_pkts)
            req_pdus = conn_pdus.loc[conn_pdus['l4_from_client'] == True]
            dset.extend(req_pdus.l4_d_size.tolist())
        #return Bestmodel().getFit(dset)
        return Allmodel().getFit(dset)
        #do it

    def model_conn_response_sizes(self, conn_class_pkts):
        LOGGER.debug('modeling connection response sizes')
        dset = []
        l4_sess_idxs = conn_class_pkts.l4_sess_idx.dropna().unique().tolist()
        for conn in l4_sess_idxs:
            conn_pkts = self.app_pkts.loc[
                                self.app_pkts['l4_sess_idx'] == conn]
            conn_pdus = Pcap2csvutils().pdus_from_pkts(conn_pkts)
            resp_pdus = conn_pdus.loc[conn_pdus['l4_from_client'] == False]
            dset.extend(resp_pdus.l4_d_size.tolist())
        #return Bestmodel().getFit(dset)
        return Allmodel().getFit(dset)
        #do it

    
    def model_num_responses_per_req(self, conn_class_pkts):
        LOGGER.debug('modeling num response per req')
        dset = []
        l4_sess_idxs = conn_class_pkts.l4_sess_idx.dropna().unique().tolist()
        for conn in l4_sess_idxs:
            conn_pkts = self.app_pkts.loc[
                                self.app_pkts['l4_sess_idx'] == conn]
            conn_pdus = Pcap2csvutils().pdus_from_pkts(conn_pkts)
            resp_count = 0
            i = 0
            for index, row in conn_pdus.iterrows():
                if row['l4_from_client']:
                    if i > 0:
                        dset.append(resp_count)
                    resp_count = 0
                else:
                    resp_count += 1
                i += 1
        LOGGER.debug('resp')
        #return Bestmodel().getFit(dset)
        return Allmodel().getFit(dset)
        #do it
        
    def model_conn_inter_request_times(self, conn_class_pkts):
        LOGGER.debug('modeling connection inter request times')
        dset = []
        l4_sess_idxs = conn_class_pkts.l4_sess_idx.dropna().unique().tolist()
        for conn in l4_sess_idxs:
            conn_pkts = self.app_pkts.loc[
                                self.app_pkts['l4_sess_idx'] == conn]
            conn_pdus = Pcap2csvutils().pdus_from_pkts(conn_pkts)
            req_pdus = conn_pdus.loc[conn_pdus['l4_from_client'] == True]
            ipts = req_pdus.rel_time.diff().tolist()[1:]
            dset.extend(ipts)
        #return Bestmodel().getFit(dset)
        return Allmodel().getFit(dset)
    
    def model_conn_server_processing_times(self, conn_class_pkts):
        LOGGER.debug('modeling connection server processing times')
        #do it
        dset = []
        l4_sess_idxs = conn_class_pkts.l4_sess_idx.dropna().unique().tolist()
        for conn in l4_sess_idxs:
            conn_pkts = self.app_pkts.loc[
                                self.app_pkts['l4_sess_idx'] == conn]
            conn_pdus = Pcap2csvutils().pdus_from_pkts(conn_pkts)
            prev_req_time = 0
            for index, row in conn_pdus.iterrows():
                if row['l4_from_client']:
                    prev_req_time = row['rel_time']
                else:
                    dset.append(row['rel_time'] - prev_req_time)
        #return Bestmodel().getFit(dset)
        return Allmodel().getFit(dset)

    def model_usess_duration(self): 
        #uservaried/infinite... modelled..
        LOGGER.debug('modeling user session durations')
        #get dataset
        dset = []
        global_starttime = self.app_pkts.iloc[0].rel_time
        global_endtime = self.app_pkts.iloc[-1].rel_time 
        global_dur = global_endtime - global_starttime
        endpt_pairs = self.app_pkts.l3_pair_id.dropna().unique().tolist()
        for endpt_pair in endpt_pairs:
            app_ep_pkts = self.app_pkts.loc[
                self.app_pkts['l3_pair_id'] == endpt_pair]
            app_ep_usess = app_ep_pkts.user_sess_idx.dropna().unique().tolist()
            for usess_idx in app_ep_usess:
                usess_pkts = app_ep_pkts.loc[
                    app_ep_pkts['user_sess_idx'] == usess_idx]
                usess_pdus = Pcap2csvutils().pdus_from_pkts(usess_pkts)
                usess_start = usess_pdus.iloc[0].rel_time
                usess_end = usess_pdus.iloc[-1].rel_time
                usess_dur = usess_end - usess_start
                dset.append(usess_dur) 
        #make model TODO change this.. to real modelling
        diff_array = [global_dur - i for i in dset]
        if max(diff_array) < USESS_DURATION_INFINITE_THRESHOLD:
            return 'infinite'
        else:
            #return Bestmodel().getFit(dset)
            return Allmodel().getFit(dset)

    def model_first_conn_class_starttime_in_usess(self, conn_class_pkts):
        #do itc
        LOGGER.debug('modeling first conn starttime in usess')
        #make dataset
        dset = []
        usess_idxs = conn_class_pkts.user_sess_idx.dropna().unique().tolist()
        for usess_idx in usess_idxs:
            usess_pkts = self.app_pkts.loc[
                                self.app_pkts['user_sess_idx'] == usess_idx]
            usess_start_time = usess_pkts.rel_time.min()
            usess_conn_class_pkts = conn_class_pkts.loc[
                                conn_class_pkts['user_sess_idx'] == usess_idx]
            conn_type_start = usess_conn_class_pkts.rel_time.min()
            first_time = conn_type_start - usess_start_time
            dset.append(first_time)
        #print(dset)
        #return Bestmodel().getFit(dset)
        return Allmodel().getFit(dset)

    def model_conn_start_starts_intervals(self, conn_class_pkts):
        #do it
        LOGGER.debug('modeling first conn starts intervals')        
        dset = []
        usess_idxs = conn_class_pkts.user_sess_idx.dropna().unique().tolist()
        for usess_idx in usess_idxs:
            pkts_usess_conn_type = conn_class_pkts.loc[ 
                (conn_class_pkts['user_sess_idx'] == usess_idx)]
            unique_conns = pkts_usess_conn_type.l4_sess_idx.dropna().unique()
            p_start = float('-inf')
            p_end = float('-inf')
            for i,conn in enumerate(unique_conns):
                conn_pkts = pkts_usess_conn_type.loc[
                                pkts_usess_conn_type['l4_sess_idx'] == conn]
                start = conn_pkts.iloc[0].rel_time
                end = conn_pkts.iloc[-1].rel_time
                start_start_interval = start - p_start
                #print (i, start, end, start_start_interval, end_start_interval)
                if start_start_interval > CONN_POOL_INTERVAL_THRESHOLD and i > 0:
                    dset.append(start_start_interval)
                p_start = start
                p_end = max(end, p_end)
        ##make model from dset
        ##print(dset)
        #ret = Bestmodel().getFit(dset)
        ret = Allmodel().getFit(dset)
        return ret
 
    def model_conn_ends_starts_intervals(self, conn_class_pkts):
        #do it
        LOGGER.debug('modeling first conn starts intervals')        
        dset = []
        usess_idxs = conn_class_pkts.user_sess_idx.dropna().unique().tolist()
        for usess_idx in usess_idxs:
            pkts_usess_conn_type = conn_class_pkts.loc[ 
                (conn_class_pkts['user_sess_idx'] == usess_idx)]
            unique_conns = pkts_usess_conn_type.l4_sess_idx.dropna().unique()
            p_start = float('-inf')
            p_end = float('-inf')
            for i,conn in enumerate(unique_conns):
                conn_pkts = pkts_usess_conn_type.loc[
                                pkts_usess_conn_type['l4_sess_idx'] == conn]
                start = conn_pkts.iloc[0].rel_time
                end = conn_pkts.iloc[-1].rel_time
                end_start_interval = start - p_end
                start_start_interval = start - p_start
                #print (i, start, end, start_start_interval, end_start_interval)
                if start_start_interval > CONN_POOL_INTERVAL_THRESHOLD and i > 0:
                    dset.append(max(0,end_start_interval))
                p_start = start
                p_end = max(end, p_end)
        #make model from dset
        #print(dset)
        #ret = Bestmodel().getFit( dset)
        ret = Allmodel().getFit( dset)
        return ret
        
    def model_num_conn_pools(self, conn_class_pkts):
        #models num_conns, assumes conns in a pool to 1 conn
        dset = []
        usess_idxs =conn_class_pkts.user_sess_idx.dropna().unique().tolist()
        for usess_idx in usess_idxs:
            pkts_usess_conn_type = conn_class_pkts.loc[ 
                (conn_class_pkts['user_sess_idx'] == usess_idx)]
            #replace nunique_conns with nunique_conn_pools
            nunique_conn_pools = pkts_usess_conn_type.conn_pool_idx.nunique()
            dset.append(nunique_conn_pools)
        #return Bestmodel().getFit(dset)
        return Allmodel().getFit(dset)

    def has_overlapping_conn_pools(self, conn_class_pkts):
        #do it
        overlap = False
        LOGGER.debug('check for overlapping conn pools')        
        dset = []
        usess_idxs = conn_class_pkts.user_sess_idx.dropna().unique().tolist()
        for usess_idx in usess_idxs:
            pkts_usess_conn_type = conn_class_pkts.loc[ 
                (conn_class_pkts['user_sess_idx'] == usess_idx)]
            unique_conns = pkts_usess_conn_type.l4_sess_idx.dropna().unique()
            p_start = float('-inf')
            p_end = float('-inf')
            for i,conn in enumerate(unique_conns):
                conn_pkts = pkts_usess_conn_type.loc[
                                pkts_usess_conn_type['l4_sess_idx'] == conn]
                start = conn_pkts.iloc[0].rel_time
                end = conn_pkts.iloc[-1].rel_time
                end_start_interval = start - p_end
                start_start_interval = start - p_start
                #print (i, start, end, start_start_interval, end_start_interval)
                if start_start_interval > CONN_POOL_INTERVAL_THRESHOLD and i > 0:
                    if end_start_interval < 0:
                        overlap = True
                        break
                    dset.append(max(0,end_start_interval))
                p_start = start
                p_end = max(end, p_end)
            if overlap:
                break
        return overlap

    def model_num_conn_in_pool(self, conn_class_pkts):
        #do it
        LOGGER.debug('modeling number of conns in conn pool')
        dset = []
        cpool_idxs = conn_class_pkts.conn_pool_idx.dropna().unique().tolist()
        for cpool_idx in cpool_idxs:
            cpool_pkts = conn_class_pkts.loc[ \
                conn_class_pkts['conn_pool_idx'] == cpool_idx]
            nunique_conns = cpool_pkts.l4_sess_idx.dropna().nunique()
            dset.append(nunique_conns)
        #make model from dset
        #return  Bestmodel().getFit(dset)
        return  Allmodel().getFit(dset)

    def model_conn_close_wait(self, conn_class_pkts):
        #do it
        LOGGER.debug('modeling number of conns in conn pool')
        dset = []
        l4_sess_idxs = conn_class_pkts.l4_sess_idx.dropna().unique().tolist()
        for l4_sess_idx in l4_sess_idxs:
            conn_pkts = conn_class_pkts.loc[ \
                conn_class_pkts['l4_sess_idx'] == l4_sess_idx]
            conn_pdus = Pcap2csvutils().pdus_from_pkts(conn_pkts)
            if len(conn_class_pkts.index) == 0 or len(conn_pdus.index)==0:
                continue
            dset.append(conn_pkts.iloc[-1].rel_time - conn_pdus.iloc[-1].rel_time)
        #make model from dset
        #return  Bestmodel().getFit(dset)
        return  Allmodel().getFit(dset)
