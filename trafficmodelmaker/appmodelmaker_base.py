'''this module helps create app model from an input pcap file.
TO DO: add more detailed description'''

import sys
import logging
import json
import random
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.stats as st
from scipy.interpolate import CubicSpline
from sklearn.metrics import mean_squared_error
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from pcap2csv.pcap2csvutils import Pcap2csvutils

LOGGER = logging.getLogger(__name__)

class Bestmodel():
    def __init__(self):
        pass

    def getFit(self, data):

        if len(data) == 0:
            return {'model_type': 'val', 'val': 0,}
        
        if len(data) == 1:
            return Average().getFit(data)

        m = pd.DataFrame(data) if isinstance(data, list) else data
        unique = len(m[m.columns[-1]].dropna().unique())
        if unique < 10:
            return Discrete().getFit(data)

        return Emperical().getFit(data)

        '''
        methods = ['Stochastic'] #, 'Average', 'Discrete', 'Discretebin', 'Regressor']
        bestmodel = None
        min_MSE = float('inf')
        max_ks2 = 0
        for methodi in methods:
            model_class = getattr(sys.modules[__name__], methodi)
            modeli = model_class().getFit(data)
            if 'ks_2_test' in modeli and modeli['ks_2_test'][1] > max_ks2:
                #print((methodi, ' ks = ', modeli['ks_2_test'][1]))
                max_ks2 = modeli['ks_2_test'][1]
                bestmodel = modeli
        #print((bestmodel['model_type']))
        #print()
        return bestmodel
        '''


class Allmodel():
    def __init__(self):
        pass

    def getFit(self, data):
        all_mods = {}
        all_mods['mtype'] = 'all-models'
        if len(data) == 0:
            return {'model_type': 'val', 'val': 0,}
        
        all_mods['average'] = Average().getFit(data)
        if len(data) == 1:
            return all_mods
        
        all_mods['emperical'] = Emperical().getFit(data)
        all_mods['stochastic'] = Stochastic().getFit(data)
        #all_mods['discretebin'] = Discretebin().getFit(data)
        #m = pd.DataFrame(data) if isinstance(data, list) else data
        #unique = len(m[m.columns[-1]].dropna().unique())
        #if unique < 20:
        #    all_mods['discrete'] = Discrete().getFit(data)
        return all_mods

class Emperical():
    def __init__(self):
        pass

    def getFit(self, data):
        #return self.ecdf(data)
        return self.percentiles(data)

    def ecdf(self, data):
        """Compute ECDF for a one-dimensional array of measurements."""
        # Number of data points: n
        n = len(data)
        x = np.sort(data)
        y = np.arange(1, n+1) / n
        sampled = self.getPercentileSamples(x, y, len(data))
        return {'model_type': 'emperical',
                'x': x.tolist(),
                'y':y.tolist(),
                'max': max(data),
                'min': min(data),
                'MSE_all': mean_squared_error(y_true=data, y_pred=sampled),
                'ks_2_test': st.ks_2samp(data, sampled)
                }

    def percentiles(self, data):
        # Specify array of percentiles: percentiles
        x = np.array(range(0,101, 5))
        y = np.percentile(data, x)
        sampled = self.getPercentileSamples(x, y, len(data))
        return {'model_type': 'emperical',
                'x': x.tolist(),
                'y':y.tolist(),
                'max': max(data),
                'min': min(data),
                'MSE_all': mean_squared_error(y_true=data, y_pred=sampled),
                'ks_2_test': st.ks_2samp(data, sampled)
               }

    def getPercentileSamples(self, x, y, num_samples):
        rands = [random.choice(range(0,101)) for i in range(num_samples)]
        cs = CubicSpline(x, y)
        vals = [cs(rand) for rand in rands]
        return vals

    def getEcdfSamples(self, x, y, num_samples):
        vals = [random.choice(x) for i in range(num_samples)]
        #cs = CubicSpline(x, y)
        #val = cs(rand)
        return vals

class Average():
    def __init__(self):
        pass

    def getFit(self, data):
        y = data
        if isinstance(y, pd.DataFrame):
            val = data.iloc[ :, -1:].dropna().mean()[0]
        elif isinstance(y, list):
            val = sum(y)/len(y)

        sampled = [val]*len(data)

        return {'model_type': 'average',
                'average_value': val,
                'max': max(data),
                'min': min(data),
                'MSE_all': mean_squared_error(y_true=y, y_pred=sampled),
                'ks_2_test': st.ks_2samp(y, sampled)
                }

class Stochastic(object):
    def __init__(self):
        self.dist_names = ['norm', 'pareto', 'weibull_min', 'weibull_max',
                           'expon', 'lognorm', 'laplace']

    def getFit(self, data, plot_nbins=30, app_name='appname'):
        y = data
        if isinstance(y, pd.DataFrame):
            y = data.iloc[ :, -1:].dropna().values.flatten().tolist()
        assert (isinstance(y, list)), "input to stochastic modeller \
                                        must be list or dataframe"
        c_name = data.columns[-1] if isinstance(data, pd.DataFrame) else 'None'
        dist_results = []
        params = {}
        col = ['red', 'blue', 'green', 'orange', 'black',
               'grey', 'purple', 'yellow']
        n_c = 0
        for dist_name in self.dist_names:
            dist = getattr(st, dist_name)
            param = dist.fit(y)
            params[dist_name] = param
            #Applying the Kolmogorov-Smirnov test
            D, p = st.kstest(y, dist_name, args=param)
            dist_results.append((dist_name,p))
            x=np.linspace(min(y), max(y), plot_nbins)
            dist_pdf = dist.pdf(x, *param[:-2], loc=param[-2], scale=param[-1])
            n_c += 1
        sel_dist,p = (max(dist_results,key=lambda item:item[1]))
        param = params[sel_dist]
        
        dist = getattr(st, sel_dist)
        sampled = dist.rvs(*param[:-2], loc=param[-2], scale=param[-1], size=len(y))
        if isinstance(y[0], int):
            sampled = [int(round(i)) for i in sampled]
        min_y = min(y)
        max_y = max(y)
        sampled = [min_y if i < min_y else i for i in sampled]
        sampled = [max_y if i > max_y else i for i in sampled]
        
        return {'model_type': 'stochastic',
                'dist_name': sel_dist, 
                'dist_params': param,
                'max': max(y),
                'min': min(y),
                'MSE_all': mean_squared_error(y_true=y, y_pred=sampled),
                'ks_2_test': st.ks_2samp(y, sampled)
                }

class Discrete(object):
    def __init__(self):
        pass

    def getFit(self, data):
        m = data
        if isinstance(m, pd.DataFrame):
            m = data.iloc[ :, -1:].dropna().values.flatten().tolist()
        assert (isinstance(m, list)), "input to stochastic modeller \
                                        must be list or dataframe"        
        df = pd.DataFrame(m).dropna()
        y = df.groupby([0]).size()/len(df)
        vals = y.index.values.ravel().tolist()
        probs = y.values.ravel().tolist()
        df=df.values.ravel()
        #print df
        
        #TO GENERATE samples
        i_x = y.index
        idx = np.arange(len(i_x))
        p_x = y.values
        dist = st.rv_discrete(name='Discrete', values=(idx, p_x))
        #plt.plot(i_x, dist.pmf(idx), 'bo', ms=12, mec='r')
        #plt.vlines(i_x, 0, dist.pmf(idx), colors='b', lw=4)
        #plt.title = data.columns[-1] + '---'
        #plt.legend(loc='upper right')
        #plt.show()
        sampled = dist.rvs(size=len(df))
        sampled = [i_x[idx] for idx in sampled]
        return {'model_type': 'discrete',
                'dist_name': 'custom', 
                'dist_params': ['val', 'prob'],
                'min':min(data),
                'max':max(data),
                'dist_values': [vals, probs],
                'MSE_all': mean_squared_error(y_true=df, y_pred=sampled),
                'ks_2_test': st.ks_2samp(df, sampled)
                }

        

class Discretebin(object):
    def __init__(self):
        pass
    
    def getFit(self, data):
        col_name = data.columns[-1]
        df = data.iloc[ :, -1:].dropna()
        df.rename(columns={col_name:0}, inplace=True)
        loglist = [0, 
                   0.01, 0.03, 0.06,
                   0.1, 0.3, 0.6, 
                   1, 3, 6, 
                   10, 30, 60, 
                   100, 300, 600,
                   1000, 3000, 6000, 
                   10000, 30000, 60000,
                   100000, 300000, 600000,
                   1000000, 3000000, 6000000,
                   10000000, 30000000, 60000000,
                   100000000, 300000000, 600000000]
        group = df.groupby(pd.cut(df[0], loglist))
        y = group.size()/len(df)
        y = pd.DataFrame(y)
        y['y_mean'] = group.mean()
        y['y_std'] = group.std(ddof=0)
        y.rename(columns={0:'y_prob'}, inplace=True)
        y.dropna(subset=['y_mean'], inplace = True)
        #print((len(y)))
        if len(y) == 0:
            return {}

        y.y_prob = y.y_prob/y.y_prob.sum()
        if y.y_prob.sum() != 1.0:
            self.logger.debug('sum of probabilities != 1.0 but' + str(y.y_prob.sum()))
            y.y_prob.iloc[-1] = y.y_prob.iloc[-1] + (1 - y.y_prob.sum())
        self.logger.debug('sum of probabilities is now' + str(y.y_prob.sum()))
        y = y.reset_index()
        
        means = y.y_mean.values.ravel().tolist()
        vals = y.y_std.values.ravel().tolist()
        probs = y.y_prob.values.ravel().tolist()
        
        df=df.values.ravel()
        
        #TO GENERATE samples
        dist = st.rv_discrete(name='Discretebin', values=(y.index, y.y_prob))
        sampled = dist.rvs(size=len(df))
        sampled = [np.random.normal(y.y_mean[i], y.y_std[i]) for i in sampled]
        
        if np.issubdtype(df.dtype, np.integer):
            sampled = [int(round(i)) for i in sampled]
        min_y = df.min()
        max_y = df.max()
        sampled = [min_y if i < min_y else i for i in sampled]
        sampled = [max_y if i > max_y else i for i in sampled]
        
        return {'model_type': 'discrete',
                'dist_name': 'custom', 
                'dist_params': ['mean_val', 'std', 'prob'],
                'dist_values': [means, vals, probs],
                'max': max_y,
                'min': min_y,
                'MSE_all': mean_squared_error(y_true=df, y_pred=sampled),
                'ks_2_test': st.ks_2samp(df, sampled)
                }

class Regressor(object):
    def __init__(self):
        pass

    def getFit(self, data):
        y_name = data.columns[-1]
        data = data.dropna()

        y = data.iloc[:, -1:].values.ravel()
        X = data.iloc[:, 1:-1].values
        #print X[:5]
        #print y[:5]
        X_train, X_test, y_train, y_test = train_test_split(X, y, random_state = 47, test_size = 0.5)
        depth = min(10, len(data)/5)
        depth = 1 if depth < 2 else depth

        clf = DecisionTreeRegressor(max_depth = depth)
        if len(X_train) == 0 or len(y_train) == 0:
            return {}
        clf.fit(X_train, y_train)

        sampled =  clf.predict(X)
        if np.issubdtype(data.iloc[ :, -1:].values.ravel().dtype, np.integer):
            sampled = [int(round(i)) for i in sampled]
        min_y = min(y)
        max_y = max(y)
        sampled = [min_y if i < min_y else i for i in sampled]
        sampled = [max_y if i > max_y else i for i in sampled]
        
        return {'model_type': 'Regressor',
                'alg_name': 'DecisionTreeRegressor', 
                'y_alg_pickle': pickle.dumps(clf),
                'x_features': data.columns[1:-1].values.tolist(),
                'max': max_y,
                'min': min_y,
                'MSE_all': mean_squared_error(y_true=y, y_pred=sampled),
                'KS_2_test':st.ks_2samp(y, sampled)
                }
